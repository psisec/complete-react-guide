import React, { Component } from "react";
import classes from "./App.css";
import Persons from "../components/Persons/Persons";
import Cockpit from "../components/Cockpit/Cockpit";
import withClass from "../hoc/withClass";
import Aux from "../hoc/Auxiliary";
import AuthContext from "../context/auth-context";

class App extends Component {
	constructor(props) {
		super(props);
		console.log("[App.js] constructor");
	}
	state = {
		showPersons: false,
		showCockpit: true,
		changeCounter: 0,
		authenticated: false,
		persons: [
			{ id: "sdfds", name: "Max", age: 28 },
			{ id: "sadfa1", name: "Manu", age: 29 },
			{ id: "2sfw2", name: "Lashko", age: 25 }
		],
		otherState: "some other value"
	};

	static getDerivedStateFromProps(props, state) {
		console.log("[App.js] getDerivedStateFromProps", props);
		return state;
	}
	// componentWillMount() {
	// 	console.log("[App.js] componentWillMount");
	// }
	componentDidMount() {
		console.log("[App.js] componentDidMount");
	}
	componentDidUpdate() {
		console.log("[App.js] componentDidUpdate");
	}
	shouldComponentUpdate(nextProps, nextState) {
		console.log("[App.js] shouldComponentUpdate");
		return true;
	}
	nameChangedHandler = (event, id) => {
		const personIndex = this.state.persons.findIndex((p) => {
			return p.id === id;
		});
		const person = {
			...this.state.persons[personIndex]
		};

		person.name = event.target.value;
		//console.log("PERSON NAME", person.name);
		const persons = [...this.state.persons];
		persons[personIndex] = person;
		//console.log("PERSONS OBJ", persons[personIndex]);
		this.setState((prevState, props) => {
			return {
				persons: persons,
				changeCounter: prevState.changeCounter + 1
			};
		});
	};

	deletePersonHandler = (personIndex) => {
		const persons = [...this.state.persons];
		persons.splice(personIndex, 1);
		this.setState({ persons: persons });
	};

	togglePersonsHandler = () => {
		const doesShow = this.state.showPersons;
		this.setState({ showPersons: !doesShow });
	};
	loginHandler = () => {
		this.setState({ authenticated: true });
	};

	render() {
		console.log("[App.js] render");
		let persons = null;

		if (this.state.showPersons) {
			persons = (
				<Persons
					persons={this.state.persons}
					clicked={this.deletePersonHandler}
					changed={this.nameChangedHandler}
					isAuthenticated={this.state.authenticated}
				/>
			);
		}

		return (
			<Aux>
				<button
					onClick={() => {
						this.setState({ showCockpit: false });
					}}
				>
					Remove Cockpit
				</button>
				<AuthContext.Provider
					value={{
						authenticated: this.state.authenticated,
						login: this.loginHandler
					}}
				>
					{this.state.showCockpit ? (
						<Cockpit
							title={this.props.appTitle}
							showPersons={this.state.showPersons}
							personsLength={this.state.persons.length}
							clicked={this.togglePersonsHandler}
						/>
					) : null}
					{persons}
				</AuthContext.Provider>
			</Aux>
		);
	}
}

export default withClass(App, classes.App);
// const style = {
// 	backgroundColor: "green",
// 	color: "white",
// 	font: "inherit",
// 	border: "1x solid blue",
// 	padding: "8px",
// 	cursor: "pointer",
// 	":hover": {
// 		backgroundColor: "lightgreen",
// 		color: "black"
// 	}
// };

//style.backgroundColor = "red";
//style[":hover"] = {
//	backgroundColor: "salmon",
//	color: "black"
//};
